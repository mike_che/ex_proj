FROM node

RUN mkdir /application
WORKDIR /application
COPY package.json /application
RUN yarn install

COPY . /application
RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000

